/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <petscksp.h>
#include "ctx.h"

/* Explicitly compute the extremal eigenvalues of a 5-point Laplacian matrix
   with Dirichlet boundary conditions, cribbing from
   http://math.ucdenver.edu/~brysmith/software/Eigenvalues_of_the_discrete_laplacian_bryan_smith.pdf
   */
#undef __FUNCT__
#define __FUNCT__ "obtainEigenvalueEstimates"
PetscErrorCode obtainEigenvalueEstimates(PetscReal *lmin,PetscReal *lmax,Ctx *ctx)
{
  const PetscReal pi = 3.141592653589793238462643383279502884197169399375105820974944592307816;
  const PetscReal N=ctx->N,M=ctx->M,hx=ctx->hx,hy=ctx->hy,oneoverhy2=1.0/(hy*hy),oneoverhx2=1.0/(hx*hx);
  PetscReal sinlox,sinloy,sinhix,sinhiy;
  
  PetscFunctionBeginUser;
  sinlox = PetscSinReal(pi     / (2*M + 2));
  sinloy = PetscSinReal(pi     / (2*N + 2));
  sinhix = PetscSinReal(pi * M / (2*M + 2));
  sinhiy = PetscSinReal(pi * N / (2*N + 2)); 
  *lmin = ( 4.0 * oneoverhx2 * sinlox * sinlox ) + ( 4.0 * oneoverhy2 * sinloy * sinloy );
  *lmax = ( 4.0 * oneoverhx2 * sinhix * sinhix ) + ( 4.0 * oneoverhy2 * sinhiy * sinhiy );

  /* An ad hoc shift */
  if(ctx->interval_shift != 0.0){
    *lmin = *lmin + ctx->interval_shift * (*lmax - *lmin);
  }
  PetscFunctionReturn(0);
}
