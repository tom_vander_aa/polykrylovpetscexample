/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <petscdmda.h> 

typedef struct
{
 DM da;
 PetscInt order;           /* Polynomial order */
 Vec work_local[4];        /* local work vectors (with ghosts) */
 PetscScalar *work_local_a[4]; /*Buffers to use with the local vectors */
 Vec work[3];              /* global work vectors */
 PetscReal hx,hy;          /* grid spacing */
 PetscInt M,N;             /* Global grid dimensions */
 PetscReal lmin,lmax;      /* (Real) bounds used to define Chebyshev polynomials. These might be shifted and 
                              thus be distinct from the eigenvalues of A */
 Mat A;                    /* The operator, only used for naive P application */
 PetscReal interval_shift; /* A parameter which shifts the lower eigenvalue estimate */
} Ctx;
