/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <petscdmda.h>
#include "ctx.h"

#undef __FUNCT__
#define __FUNCT__ "assembleA"
PetscErrorCode assembleA(Mat A, Ctx *ctx)
{
  PetscErrorCode ierr;
  PetscInt i,j,ixs,iys,ixm,iym;
  MatStencil row,col[5];
  PetscScalar v[5];
  DM da=ctx->da;
  PetscInt M=ctx->M,N=ctx->N;
  PetscReal hx=ctx->hx,hy=ctx->hy,oneoverhx2,oneoverhy2;

  PetscFunctionBeginUser;

  /* Note: the below likely still needs optimization if it is to be used in 
     comparison to our method, as opposed to just for debugging */

  oneoverhx2 = 1.0/(hx*hx);
  oneoverhy2 = 1.0/(hy*hy);

  ierr=DMDAGetCorners(da,&ixs,&iys,0,&ixm,&iym,0);CHKERRQ(ierr);

  for (j=iys; j<iys+iym; ++j){
    for (i=ixs; i<ixs+ixm; ++i){
      row.i=i; row.j=j;
      col[0].i = i;   
      col[0].j = j;
      v[0] = 2.0*(oneoverhx2 + oneoverhy2); 
      if (i==0 || j==0 || i==M-1 || j==N-1) {
        PetscInt num=1;
        if (j!=0) {
          v[num] = -oneoverhy2; col[num].i = i;   col[num].j = j-1;
          num++;
        }
        if (i!=0) {
          v[num] = -oneoverhx2; col[num].i = i-1; col[num].j = j;
          num++;
        }
        if (i!=M-1) {
          v[num] = -oneoverhx2; col[num].i = i+1; col[num].j = j;
          num++;
        }
        if (j!=N-1) {
          v[num] = -oneoverhy2; col[num].i = i;   col[num].j = j+1;
          num++;
        }
        ierr = MatSetValuesStencil(A,1,&row,num,col,v,INSERT_VALUES);CHKERRQ(ierr);
      } else {
        v[1] =     -oneoverhy2; col[1].i = i;     col[1].j = j-1;
        v[2] =     -oneoverhx2; col[2].i = i-1;   col[2].j = j;
        v[3] =     -oneoverhx2; col[3].i = i+1;   col[3].j = j;
        v[4] =     -oneoverhy2; col[4].i = i;     col[4].j = j+1;
        ierr = MatSetValuesStencil(A,1,&row,5,col,v,INSERT_VALUES);CHKERRQ(ierr);
      }
    }
  }

  ierr=MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr=MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
