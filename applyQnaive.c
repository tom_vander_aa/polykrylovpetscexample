/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <petscvec.h>
#include <petscmat.h>
#include "ctx.h"

#undef __FUNCT__
#define __FUNCT__ "applyQnaive"
PetscErrorCode applyQnaive(Mat P, Vec in, Vec out)
{
  PetscErrorCode ierr;
  Ctx *ctx;
  PetscReal sigma1,theta,thetainv,delta,rho,rhoprev;
  PetscInt k;
  Vec v,w,dw,tmp;
  Mat A;

  PetscFunctionBeginUser;

  ierr = MatShellGetContext(P, &ctx);CHKERRQ(ierr);
  A    = ctx->A;
  w    = out;
  dw   = ctx->work[0];
  tmp  = ctx->work[1];
  v    = in;

  /* theta and delta are the center and half-width of the interval */
  theta    = 0.5 * (ctx->lmax + ctx->lmin);
  thetainv = 1.0 / theta;
  delta    = 0.5 * (ctx->lmax - ctx->lmin);
  sigma1   = theta/delta;

  /* w_1  = (1/theta)v (one less A here) */
  ierr = VecCopy(v,w);CHKERRQ(ierr);
  ierr = VecScale(w,thetainv);CHKERRQ(ierr); 
  /* Note that this differs from I by a scaling for order 1. */

  if(ctx->order > 1){
    /* dw_1 = w_1 */
    ierr = VecCopy(w, dw);CHKERRQ(ierr);

    rho = 1.0 / sigma1;
    for(k=1;k<ctx->order;++k){
      rhoprev = rho;
      rho = 1.0 / (2.0 * sigma1 - rhoprev);

      /* tmp <--  v - Aw (One less A here)*/
      ierr = MatMult(A,w,tmp);CHKERRQ(ierr);
      ierr = VecAXPBY(tmp,1.0,-1.0,v);CHKERRQ(ierr);

      /* dw <-- 2 rho/delta tmp  + rho rhoprev dw */
      ierr = VecAXPBY(dw, 2.0 * rho / delta, rho * rhoprev, tmp);CHKERRQ(ierr);

      /* w <-- w + deltaw */
      ierr = VecAXPY(w,1.0,dw);CHKERRQ(ierr);
    }
  }

  PetscFunctionReturn(0);
}
