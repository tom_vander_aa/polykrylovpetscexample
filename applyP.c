/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan and Simplice Donfack

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <petscvec.h>
#include <petscmat.h>
#include "ctx.h"

/* Note : this function contains a large amount of duplicated code, 
          in the interest of allowing source-to-source optimization
          to proceed smoothly */

/* Define PKDEMO_USE_BUFFER to use extra buffers */

extern PetscErrorCode DMDAVecGetArray3dView(DM,Vec,void*,void*);
extern PetscErrorCode DMDAVecRestoreArray3dView(DM,Vec,void*);

#undef __FUNCT__
#define __FUNCT__ "applyP"
PetscErrorCode applyP(Mat P,Vec in,Vec out)
{
  PetscErrorCode ierr;
  Ctx *ctx;
  PetscReal oneoverhy2,oneoverhx2,sigma1,theta,thetainv,delta,rho,rhoprev,twooverdelta,*alpha,*beta;
  PetscInt i,j,k,ixs,iys,ixm,iym,imin,imax,jmin,jmax,m,N,M;
  Vec v,w,dw;
  const PetscScalar **varr;
  PetscScalar **outarr,**warr,**dwarr;
#ifdef PKDEMO_USE_BUFFER
  Vec Av; /* TODO consider using this formulation even with the unbuffered case */
  PetscScalar	**Avarr;
#endif

#ifdef PKDEMO_USE_BUFFER
  PetscScalar ***warr_b,***dwarr_b; 
  PetscScalar qhxhy;
#endif
  PetscBool left,right,up,down;

  PetscFunctionBeginUser;
  ierr = MatShellGetContext(P,&ctx);CHKERRQ(ierr);
  m           =ctx->order;
  oneoverhy2  = 1.0/(ctx->hy*ctx->hy); 
  oneoverhx2  = 1.0/(ctx->hx*ctx->hx);
  M           = ctx->M;
  N           = ctx->N;

  /* Assign names to work vectors (all with ghost points) */
  v    = ctx->work_local[0]; 
  w    = ctx->work_local[1];
  dw   = ctx->work_local[2];
#ifdef PKDEMO_USE_BUFFER
  Av   = ctx->work_local[3];
#endif
  /* theta and delta are the center and half-width of the interval */
  theta        = 0.5 * (ctx->lmax + ctx->lmin);
  thetainv     = 1.0 / theta;
  delta        = 0.5 * (ctx->lmax - ctx->lmin);
  sigma1       = theta/delta;
  twooverdelta = 2.0 / delta;

  /* Precompute coefficients to appease the stencil compilers */
  ierr=PetscMalloc2(m*sizeof(PetscReal),&alpha,m*sizeof(PetscReal),&beta);CHKERRQ(ierr);
  rho = 1.0 / sigma1;
  for(k=1;k<m;++k){
    rhoprev = rho;
    rho = 1.0 / (2.0 * sigma1 - rhoprev);
    alpha[k] = rho * twooverdelta;
    beta[k]  = rho * rhoprev;
  }

  /* Scatter input vector global to local to obtain the ghost values.
     Note that since we have used the box stencil, there are
     4 * order * (order-1) unused values here. This is unimportant
     in most cases, and simplifies the loop below, but might be important
     in the strong-scaling limit, which indeed might be of interest for this
     project */
  ierr=DMGlobalToLocalBegin(ctx->da,in,INSERT_VALUES,v);CHKERRQ(ierr);
  ierr=DMGlobalToLocalEnd  (ctx->da,in,INSERT_VALUES,v);CHKERRQ(ierr);

  /* Get the boundaries of the local subdomain (without ghosts) */
  DMDAGetCorners(ctx->da,&ixs,&iys,0,&ixm,&iym,0);CHKERRQ(ierr);

  /* Determine active (global) boundaries and boundaries of the "interior"
     where a single branch-free loop nest can be applied (and optimized)
     This rests on the assumption that subdomains are at least as wide
     as the overlap (which is equal to the polynomial order)
   */
  up   = (iys       == 0); jmin = up   ? iys       + m : iys;
  down = (iys + iym == N); jmax = down ? iys + iym - m : iys + iym; 
  left = (ixs       == 0); imin = left ? ixs       + m : ixs; 
  right= (ixs + ixm == M); imax = right? ixs + ixm - m : ixs + ixm;

  /* Get access to the raw arrays (with ghosts for the input array). 
     Note that PETSc allows these to be accessed with *global* indices */
  DMDAVecGetArray(ctx->da,out,&outarr);CHKERRQ(ierr);
  DMDAVecGetArrayRead(ctx->da,v,&varr);CHKERRQ(ierr);
#ifdef PKDEMO_USE_BUFFER
  DMDAVecGetArrayRead(ctx->da,Av,&Avarr);CHKERRQ(ierr);
#endif
  DMDAVecGetArray(ctx->da,w,&warr);CHKERRQ(ierr);
  DMDAVecGetArray(ctx->da,dw,&dwarr);CHKERRQ(ierr);

  /* These vectors should have been allocated in a special way wherein
     they have extra contiguous memory. We call a custom function, 
     which will (for a 2d DMDA) return a *3*-dimensional array.
     The first/slowest index defines a buffer id */
#ifdef PKDEMO_USE_BUFFER
  DMDAVecGetArray3dView(ctx->da,w,warr,&warr_b);CHKERRQ(ierr);
  DMDAVecGetArray3dView(ctx->da,dw,dwarr,&dwarr_b);CHKERRQ(ierr);
#endif

  /* === INTERIOR ======================================================== */
  /* This and other loops refer to algorithm 3 in the paper. Also see
     applyPnaive(), which performs the same operations with less redundancy 
     and more communication steps */

  /**** THIS IS THE FIRST PLACE TO APPLY PLUTO *****************************/
#ifdef PKDEMO_USE_BUFFER
  /* Here, we use two buffers 
     warr_b[0] is the same as warr, and similarly for dwarr */
  qhxhy = 2.0*(oneoverhy2 + oneoverhx2)/oneoverhy2;
  {
    /* The first iteration */
    {
      for(j=jmin-m+1;j<jmax+m-1;++j){
        for(i=imin-m+1;i<imax+m-1;++i){
          Avarr[j  ][i  ] = oneoverhy2*(varr[j][i] * qhxhy - varr[j-1][i] - varr[j][i-1] - varr[j][i+1]-varr[j+1][i]);
          warr_b[0][j][i]= Avarr[j][i] * thetainv;
          dwarr_b[0][j][i]=warr_b[0][j][i];
        }
      }
    }

    /* Subsequent iterations */
    {
      for(k=1;k<m;++k){
        for(j=jmin-m+1+k;j<jmax+m-1-k;++j){
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            dwarr_b[k%2][j][i]=
              alpha[k] * 
              (Avarr[j][i]  - 
               (
                (
                  warr_b[(k-1)%2][j  ][i  ] * qhxhy
                 -warr_b[(k-1)%2][j-1][i  ]       
                 -warr_b[(k-1)%2][j  ][i-1] 
                 -warr_b[(k-1)%2][j  ][i+1] 
                 -warr_b[(k-1)%2][j+1][i  ] 
                )*oneoverhy2
               )
              )
              + beta[k] * dwarr_b[(k-1)%2][j][i];
            warr_b[k%2][j][i]=warr_b[(k-1)%2][j][i] + dwarr_b[k%2][j][i];
          }
        }
      }
    }
    /* Copy values to the output array. This takes the place of a local-->global scatter */
    {
      for(j=jmin;j<jmax;++j){
        for(i=imin;i<imax;++i){
          outarr[j][i] = warr_b[(m-1)%2][j][i];
        }
      }
    }
  }
#else
  {
    /* The first iteration */
    {
      for(j=jmin-m+1;j<jmax+m-1;++j){
        for(i=imin-m+1;i<imax+m-1;++i){
          warr[j][i]=
            varr[j-1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i-1] *       (-oneoverhx2)        * thetainv
            +varr[j  ][i+1] *       (-oneoverhx2)        * thetainv
            +varr[j+1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
    }
    /* Subsequent iterations */
    {
      for(k=1;k<m;++k){
        for(j=jmin-m+1+k;j<jmax+m-1-k;++j){
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            dwarr[j][i]=
              alpha[k] * (
                 (varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2)
                +(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2)
                +(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2)
                +(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2)
                +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
              ) 
              + beta[k] * dwarr[j][i];
          }
        }
        for(j=jmin-m+1+k;j<jmax+m-1-k;++j){
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    /* Copy values to the output array. This takes the place of a local-->global scatter */
    {
      for(j=jmin;j<jmax;++j){
        for(i=imin;i<imax;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }
#endif

  /****************************************************************************/
  /* Naive Implementations for the corner blocks */
  /* TODO: Implement the corner blocks without branches */
  /* TODO: we can comment out some of the conditionals in the corners */
  /* TODO: The corner cases (with branches) could be further optimized by using 
           > or < instead of !=, and\or by using a filter of the form  
           if(i == 0 || i==M-1 || j==0 || j==N-1) */

  /* === TOP LEFT CORNER ==================================================== */ 
  if(up && left){ /* 0 <= j < m, 0 <= i < m */
    const PetscInt jl=0;
    const PetscInt jr=2*m-1;
    const PetscInt il=0;
    const PetscInt ir=2*m-1;
    for(j=jl;j<jr;++j){
      for(i=il;i<ir;++i){
        PetscScalar val=0;
        if(j != 0)  {val+=varr[j-1][i  ] *       (-oneoverhy2)        * thetainv;}
        if(i != 0)  {val+=varr[j  ][i-1] *       (-oneoverhx2)        * thetainv;}
        if(i != M-1){val+=varr[j  ][i+1] *       (-oneoverhx2)        * thetainv;}
        if(j != N-1){val+=varr[j+1][i  ] *       (-oneoverhy2)        * thetainv;}
        val+=varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
        warr[j][i]=val;
        dwarr[j][i]=val;
      }
    }
    {
      rho = 1.0 / sigma1;
      for(k=1;k<m;++k){
        rhoprev = rho;
        rho = 1.0 / (2.0 * sigma1 - rhoprev);
        const PetscInt jl=0;
        const PetscInt jr=2*m-1-k;
        const PetscInt il=0;
        const PetscInt ir=2*m-1-k;
        for(j=jl;j<jr;++j){
          for(i=il;i<ir;++i){
            PetscScalar val=0;   
            if(j != 0)  {val+=(varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2);}
            if(i != 0)  {val+=(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2);}
            if(i != M-1){val+=(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2);}
            if(j != N-1){val+=(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2);}
            val+=(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2);
            val = rho * ((twooverdelta * val) + (rhoprev * dwarr[j][i]));
            dwarr[j][i]=val;
          }
        }
        for(j=jl;j<jr;++j){
          for(i=il;i<ir;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    {
      const PetscInt jl=0;
      const PetscInt jr=m;
      const PetscInt il=0;
      const PetscInt ir=m;
      for(j=jl;j<jr;++j){
        for(i=il;i<ir;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }

  /* === TOP RIGHT CORNER =================================================== */
  if(up && right){ /* 0 <= j < m, M-m <= i < M */
    const PetscInt jl=0;
    const PetscInt jr=2*m-1;
    const PetscInt il=M-2*m+1;
    const PetscInt ir=M;
    for(j=jl;j<jr;++j){
      for(i=il;i<ir;++i){
        PetscScalar val=0;
        if(j != 0)  {val+=varr[j-1][i  ] *       (-oneoverhy2)        * thetainv;}
        if(i != 0)  {val+=varr[j  ][i-1] *       (-oneoverhx2)        * thetainv;}
        if(i != M-1){val+=varr[j  ][i+1] *       (-oneoverhx2)        * thetainv;}
        if(j != N-1){val+=varr[j+1][i  ] *       (-oneoverhy2)        * thetainv;}
        val+=varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
        warr[j][i]=val;
        dwarr[j][i]=val;
      }
    }
    {
      rho = 1.0 / sigma1;
      for(k=1;k<m;++k){
        rhoprev = rho;
        rho = 1.0 / (2.0 * sigma1 - rhoprev);
        const PetscInt jl=0;
        const PetscInt jr=2*m-1-k;
        const PetscInt il=M-2*m+1+k;
        const PetscInt ir=M;
        for(j=jl;j<jr;++j){
          for(i=il;i<ir;++i){
            PetscScalar val=0;   
            if(j != 0)  {val+=(varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2);}
            if(i != 0)  {val+=(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2);}
            if(i != M-1){val+=(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2);}
            if(j != N-1){val+=(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2);}
            val+=(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2);
            val = rho * ((twooverdelta * val) + (rhoprev * dwarr[j][i]));
            dwarr[j][i]=val;
          }
        }
        for(j=jl;j<jr;++j){
          for(i=il;i<ir;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    {
      const PetscInt jl=0;
      const PetscInt jr=m;
      const PetscInt il=M-m;
      const PetscInt ir=M;
      for(j=jl;j<jr;++j){
        for(i=il;i<ir;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }
  
  /* === BOTTOM LEFT CORNER ================================================ */ 
  if(down && left){ /* N-m <= j < N,  0 <= i < m */
    const PetscInt jl=N-2*m+1;
    const PetscInt jr=N;
    const PetscInt il=0;
    const PetscInt ir=2*m-1;
    for(j=jl;j<jr;++j){
      for(i=il;i<ir;++i){
        PetscScalar val=0;
        if(j != 0)  {val+=varr[j-1][i  ] *       (-oneoverhy2)        * thetainv;}
        if(i != 0)  {val+=varr[j  ][i-1] *       (-oneoverhx2)        * thetainv;}
        if(i != M-1){val+=varr[j  ][i+1] *       (-oneoverhx2)        * thetainv;}
        if(j != N-1){val+=varr[j+1][i  ] *       (-oneoverhy2)        * thetainv;}
        val+=varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
        warr[j][i]=val;
        dwarr[j][i]=val;
      }
    }
    {
      rho = 1.0 / sigma1;
      for(k=1;k<m;++k){
        rhoprev = rho;
        rho = 1.0 / (2.0 * sigma1 - rhoprev);
        const PetscInt jl=N-2*m+1+k;
        const PetscInt jr=N;
        const PetscInt il=0;
        const PetscInt ir=2*m-1-k;
        for(j=jl;j<jr;++j){
          for(i=il;i<ir;++i){
            PetscScalar val=0;   
            if(j != 0)  {val+=(varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2);}
            if(i != 0)  {val+=(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2);}
            if(i != M-1){val+=(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2);}
            if(j != N-1){val+=(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2);}
            val+=(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2);
            val = rho * ((twooverdelta * val) + (rhoprev * dwarr[j][i]));
            dwarr[j][i]=val;
          }
        }
        for(j=jl;j<jr;++j){
          for(i=il;i<ir;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    {
      const PetscInt jl=N-m;
      const PetscInt jr=N;
      const PetscInt il=0;
      const PetscInt ir=m;
      for(j=jl;j<jr;++j){
        for(i=il;i<ir;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }

  /*  === BOTTOM RIGHT CORNER ============================================== */
  if(down && right){ /* N-m <= j < N,  M-m <= i < M */
    const PetscInt jl=N-2*m+1;
    const PetscInt jr=N;
    const PetscInt il=M-2*m+1;
    const PetscInt ir=M;
    for(j=jl;j<jr;++j){
      for(i=il;i<ir;++i){
        PetscScalar val=0;
        if(j != 0)  {val+=varr[j-1][i  ] *       (-oneoverhy2)        * thetainv;}
        if(i != 0)  {val+=varr[j  ][i-1] *       (-oneoverhx2)        * thetainv;}
        if(i != M-1){val+=varr[j  ][i+1] *       (-oneoverhx2)        * thetainv;}
        if(j != N-1){val+=varr[j+1][i  ] *       (-oneoverhy2)        * thetainv;}
        val+=varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
        warr[j][i]=val;
        dwarr[j][i]=val;
      }
    }
    {
      rho = 1.0 / sigma1;
      for(k=1;k<m;++k){
        rhoprev = rho;
        rho = 1.0 / (2.0 * sigma1 - rhoprev);
        const PetscInt jl=N-2*m+1+k;
        const PetscInt jr=N;
        const PetscInt il=M-2*m+1+k;
        const PetscInt ir=M;
        for(j=jl;j<jr;++j){
          for(i=il;i<ir;++i){
            PetscScalar val=0;   
            if(j != 0)  {val+=(varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2);}
            if(i != 0)  {val+=(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2);}
            if(i != M-1){val+=(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2);}
            if(j != N-1){val+=(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2);}
            val+=(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2);
            val = rho * ((twooverdelta * val) + (rhoprev * dwarr[j][i]));
            dwarr[j][i]=val;
          }
        }
        for(j=jl;j<jr;++j){
          for(i=il;i<ir;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    {
      const PetscInt jl=N-m;
      const PetscInt jr=N;
      const PetscInt il=M-m;
      const PetscInt ir=M;
      for(j=jl;j<jr;++j){
        for(i=il;i<ir;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }

  /* === TOP BOUNDARY ====================================================== */
  if (up){ /*  0 <= j < m */
    {
      for(j=1;j<2*m-1;++j){ /* Note j lower bound */
        for(i=imin-m+1;i<imax+m-1;++i){
          warr[j][i]=
             varr[j-1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i-1] *       (-oneoverhx2)        * thetainv
            +varr[j  ][i+1] *       (-oneoverhx2)        * thetainv
            +varr[j+1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
      {
        j=0; 
        for(i=imin-m+1;i<imax+m-1;++i){
          warr[j][i]=
             varr[j  ][i-1] *       (-oneoverhx2)        * thetainv
            +varr[j  ][i+1] *       (-oneoverhx2)        * thetainv
            +varr[j+1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
    }
    {
      for(k=1;k<m;++k){
        for(j=1;j<2*m-1-k;++j){ /* Note j lower bound */
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            dwarr[j][i] = alpha[k]* (
               (varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2)       
              +(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2)       
              +(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
            ) + (beta[k] * dwarr[j][i]);
          }
        }
        {
          j=0;
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            dwarr[j][i] = alpha[k] * (
               (varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2)       
              +(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2)       
              +(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
            ) + (beta[k] * dwarr[j][i]);
          }
        }
        for(j=0;j<2*m-1-k;++j){
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    {
      for(j=0;j<m;++j){
        for(i=imin;i<imax;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }

  /* === BOTTOM BOUNDARY =================================================== */
  if (down){ /* N-m <= j < N */
    {
      for(j=N-2*m+1;j<N-1;++j){ /* Note j upper bound */
        for(i=imin-m+1;i<imax+m-1;++i){
          warr[j][i]=
             varr[j-1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i-1] *       (-oneoverhx2)        * thetainv
            +varr[j  ][i+1] *       (-oneoverhx2)        * thetainv
            +varr[j+1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
      {
        j=N-1; 
        for(i=imin-m+1;i<imax+m-1;++i){
          warr[j][i]=
             varr[j-1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i-1] *       (-oneoverhx2)        * thetainv
            +varr[j  ][i+1] *       (-oneoverhx2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
    }
    {
      for(k=1;k<m;++k){
        for(j=N-2*m+1+k;j<N-1;++j){ /* Note adjustment to j upper bound */
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            dwarr[j][i] = alpha[k] * (
               (varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2)       
              +(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2)       
              +(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
            ) + (beta[k] * dwarr[j][i]);
          }
        }
        {
          j=N-1; /* = N-1 */
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            dwarr[j][i] = alpha[k] * (
               (varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2)       
              +(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2)       
              +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
            ) + (beta[k] * dwarr[j][i]);
          }
        }
        for(j=N-2*m+1+k;j<N;++j){
          for(i=imin-m+1+k;i<imax+m-1-k;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    {
      for(j=N-m;j<N;++j){
        for(i=imin;i<imax;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }

  /* === LEFT BOUNDARY ===================================================== */
  if (left){ /* 0 <= i < m */
    {  
      for(j=jmin-m+1;j<jmax+m-1;++j){ 
        for(i=1;i<2*m-1;++i){/* Note i lower bound */
          warr[j][i]=
             varr[j-1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i-1] *       (-oneoverhx2)        * thetainv
            +varr[j  ][i+1] *       (-oneoverhx2)        * thetainv
            +varr[j+1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
      {
        i = 0;
        for(j=jmin-m+1;j<jmax+m-1;++j){
          warr[j][i]=
             varr[j-1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i+1] *       (-oneoverhx2)        * thetainv
            +varr[j+1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
    }
    {
      for(k=1;k<m;++k){
        for(j=jmin-m+1+k;j<jmax+m-1-k;++j){
          for(i=0+1;i<2*m-1;++i){ /* Note adjustment to i */
            dwarr[j][i] = alpha[k] * (
               (varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2)       
              +(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2)       
              +(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
            ) + (beta[k] * dwarr[j][i]);
          }
        }
        {
          i = 0; /* =0 */
          for(j=jmin-m+1+k;j<jmax+m-1-k;++j){
            dwarr[j][i] = alpha[k] * (
               (varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2)       
              +(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
            ) + (beta[k] * dwarr[j][i]);
          }
        }
        for(j=jmin-m+1+k;j<jmax+m-1-k;++j){
          for(i=0;i<2*m-1;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    {
      for(j=jmin;j<jmax;++j){
        for(i=0;i<m;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }

  /* === RIGHT BOUNDARY ==================================================== */
  if (right){ /* M-m <= i < M */
    {
      for(j=jmin-m+1;j<jmax+m-1;++j){ 
        for(i=M-2*m+1;i<M-1;++i){/* Note i upper bound */
          warr[j][i]=
             varr[j-1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i-1] *       (-oneoverhx2)        * thetainv
            +varr[j  ][i+1] *       (-oneoverhx2)        * thetainv
            +varr[j+1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
      {
        i = M-1; /* = ir-1 */
        for(j=jmin-m+1;j<jmax+m-1;++j){
          warr[j][i]=
             varr[j-1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i-1] *       (-oneoverhx2)        * thetainv
            +varr[j+1][i  ] *       (-oneoverhy2)        * thetainv
            +varr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2) * thetainv;
          dwarr[j][i]=warr[j][i];
        }
      }
    }
    {
      for(k=1;k<m;++k){
        for(j=jmin-m+1;j<jmax+m-1;++j){
          for(i=M-2*m+k+1;i<M-1;++i){ /* Note adjustment to r upper bound */
            dwarr[j][i] = alpha[k] * (
               (varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2)       
              +(varr[j  ][i+1] - warr[j  ][i+1]) *       (-oneoverhx2)       
              +(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
            ) + (beta[k] * dwarr[j][i]);
          }
        }
        {
          i = M-1; /* = M-1 */
          for(j=jmin-m+1;j<jmax+m-1;++j){
            dwarr[j][i] = alpha[k] * (
               (varr[j-1][i  ] - warr[j-1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i-1] - warr[j  ][i-1]) *       (-oneoverhx2)       
              +(varr[j+1][i  ] - warr[j+1][i  ]) *       (-oneoverhy2)       
              +(varr[j  ][i  ] - warr[j  ][i  ]) * 2.0 * (oneoverhy2 + oneoverhx2)
            ) + (beta[k] * dwarr[j][i]);
          }
        }
        for(j=jmin-m+1;j<jmax+m-1;++j){
          for(i=M-2*m+k+1;i<M;++i){
            warr[j][i]+=dwarr[j][i];
          }
        }
      }
    }
    {
      for(j=jmin;j<jmax;++j){
        for(i=M-m;i<M;++i){
          outarr[j][i] = warr[j][i];
        }
      }
    }
  }

  /* Restore access to the raw arrays and views*/
  DMDAVecRestoreArray(ctx->da,out,&outarr);CHKERRQ(ierr);
  DMDAVecRestoreArray(ctx->da,w,&warr);CHKERRQ(ierr);
  DMDAVecRestoreArray(ctx->da,dw,&dwarr);CHKERRQ(ierr);
#ifdef PKDEMO_USE_BUFFER
  DMDAVecRestoreArray3dView(ctx->da,w,&warr_b);CHKERRQ(ierr);
  DMDAVecRestoreArray3dView(ctx->da,dw,&dwarr_b);CHKERRQ(ierr);
  DMDAVecRestoreArrayRead(ctx->da,Av,&Avarr);CHKERRQ(ierr);
#endif
  DMDAVecRestoreArrayRead(ctx->da,v,&varr);CHKERRQ(ierr);

  /* Free Memory */
  PetscFree2(alpha,beta);

  PetscFunctionReturn(0);
}
