/*
    Polynomially-preconditioned Krylov Solver Scalable Toy
    Copyright (C) 2015 Patrick Sanan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Here we define helper functions to create non-standard vectors associated with DAs. */

#include <petsc/private/dmdaimpl.h>    /*I   "petscdmda.h"   I*/

/*Note: Not tested for any value other than 2 */
#define PKDEMO_BUFFER_FACTOR 2

#undef __FUNCT__
#define __FUNCT__ "DMCreateLocalVectorBuffered"
PetscErrorCode DMCreateLocalVectorBuffered(DM da,Vec* g,PetscScalar **a)
{
  /* cribbed from src/dm/impls/da/dalocal.c */
  PetscErrorCode ierr;
  DM_DA          *dd = (DM_DA*)da->data;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(da,DM_CLASSID,1);
  PetscValidPointer(g,2);

  /* We create our own array here. This allows us to allocate extra space,
     contiguous with the storage used by the vector, but retain all the 
     DA functionality . Note that although a is of size PKDEMO_BUFFER_FACTOR * dd->nlocal,
     the vector is given a size of dd->nlocal
     Note: This must be freed manually !*/
  ierr = PetscMalloc1(PKDEMO_BUFFER_FACTOR * dd->nlocal,a);CHKERRQ(ierr);

  ierr = VecCreateMPIWithArray(PETSC_COMM_SELF,dd->w,dd->nlocal,PETSC_DECIDE,*a,g);CHKERRQ(ierr);
  ierr = VecSetBlockSize(*g,dd->w);CHKERRQ(ierr);
  /* Note: the code this is based on has
    ierr = VecSetType(*g,da->vectype);CHKERRQ(ierr);
    and there are potential bugs here since we do not use this!
    */
  ierr = VecSetDM(*g, da);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* Helper functions to construct and destroy 3d "views" onto array data 
   These are very similar to Vec[Get\Restore]Array3d, except that they piggyback on another "real" call
   to obtain the array and rely on the vector having been specially allocated with extra space.
   Thus, there is no call to VecGetArray here - all that is allocated is a new array of pointers
  */
#undef __FUNCT__
#define __FUNCT__ "VecGetArray3dView"
static PetscErrorCode  VecGetArray3dView(Vec x,PetscInt m,PetscInt n,PetscInt p,PetscInt mstart,PetscInt nstart,PetscInt pstart,PetscScalar *aa, PetscScalar ***a[])
{
  PetscErrorCode ierr;
  PetscInt       i,N,j;
  PetscScalar    **b;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(x,VEC_CLASSID,1);
  PetscValidPointer(a,8);
  PetscValidPointer(aa,8);
  PetscValidType(x,1);
  ierr = VecGetLocalSize(x,&N);CHKERRQ(ierr);
  /* Note that we check the size only wrt the last two arguments. The first is the buffer count (2 at the time of this writing) */
  if (n*p != N) SETERRQ3(PETSC_COMM_SELF,PETSC_ERR_ARG_INCOMP,"Local array size %D does not match 2d array dimensions %D by %D",N,n,p);

  ierr = PetscMalloc1(m*sizeof(PetscScalar**)+m*n,a);CHKERRQ(ierr);
  b    = (PetscScalar**)((*a) + m);
  for (i=0; i<m; i++) (*a)[i] = b + i*n - nstart;
  for (i=0; i<m; i++)
    for (j=0; j<n; j++)
      b[i*n+j] = aa + i*n*p + j*p - pstart;

  *a -= mstart;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecRestoreArray3dView"
static PetscErrorCode  VecRestoreArray3dView(Vec x,PetscInt m,PetscInt n,PetscInt p,PetscInt mstart,PetscInt nstart,PetscInt pstart,PetscScalar ***a[])
{
  PetscErrorCode ierr;
  void           *dummy;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(x,VEC_CLASSID,1);
  PetscValidPointer(a,8);
  PetscValidType(x,1);
  dummy = (void*)(*a + mstart);
  ierr  = PetscFree(dummy);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* After already having obtained access to a vectors array, obtain a second view
   which allows access to (assumed-to-exist) extra contiguous space */
#undef __FUNCT__
#define __FUNCT__ "DMDAVecGetArray3dView"
PetscErrorCode  DMDAVecGetArray3dView(DM da,Vec vec,void *aa,void *array)
{
  PetscErrorCode ierr;
  PetscInt       xs,ys,zs,xm,ym,zm,gxs,gys,gzs,gxm,gym,gzm,N,dim,dof;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(da, DM_CLASSID, 1);
  PetscValidHeaderSpecific(vec, VEC_CLASSID, 2);
  PetscValidPointer(array, 3);
  if (da->defaultSection) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Not supported");
  }
  ierr = DMDAGetCorners(da,&xs,&ys,&zs,&xm,&ym,&zm);CHKERRQ(ierr);
  ierr = DMDAGetGhostCorners(da,&gxs,&gys,&gzs,&gxm,&gym,&gzm);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,&dim,0,0,0,0,0,0,&dof,0,0,0,0,0);CHKERRQ(ierr);

  /* Handle case where user passes in global vector as opposed to local */
  ierr = VecGetLocalSize(vec,&N);CHKERRQ(ierr);
  if (N == xm*ym*zm*dof) {
    gxm = xm;
    gym = ym;
    gzm = zm;
    gxs = xs;
    gys = ys;
    gzs = zs;
  } else if (N != gxm*gym*gzm*dof) SETERRQ3(PETSC_COMM_SELF,PETSC_ERR_ARG_INCOMP,"Vector local size %D is not compatible with DMDA local sizes %D %D\n",N,xm*ym*zm*dof,gxm*gym*gzm*dof);

  if (dim == 2) {
    /* Note the ugly manipulation performed on aa, to obtain 
       the actual pointer to the start of the vector data. 
       This is what would be returned by VecGetArray, but
       this has already been called to obtain aa by  VecGetArray2D (through the DMDA interface)
       so we undo the manipulations there */
    ierr = VecGetArray3dView(vec,PKDEMO_BUFFER_FACTOR,gym,gxm*dof,0,gys,gxs*dof,((PetscScalar**)aa)[gys]+(gxs*dof),(PetscScalar****)array);CHKERRQ(ierr);
  } else SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMDA dimension not 2, it is %D\n",dim);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAVecRestoreArray3dView"
PetscErrorCode  DMDAVecRestoreArray3dView(DM da,Vec vec,void *array)
{
  PetscErrorCode ierr;
  PetscInt       xs,ys,zs,xm,ym,zm,gxs,gys,gzs,gxm,gym,gzm,N,dim,dof;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(da, DM_CLASSID, 1);
  PetscValidHeaderSpecific(vec, VEC_CLASSID, 2);
  PetscValidPointer(array, 3);
  if (da->defaultSection) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Not supported");
  }
  ierr = DMDAGetCorners(da,&xs,&ys,&zs,&xm,&ym,&zm);CHKERRQ(ierr);
  ierr = DMDAGetGhostCorners(da,&gxs,&gys,&gzs,&gxm,&gym,&gzm);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,&dim,0,0,0,0,0,0,&dof,0,0,0,0,0);CHKERRQ(ierr);

  /* Handle case where user passes in global vector as opposed to local */
  ierr = VecGetLocalSize(vec,&N);CHKERRQ(ierr);
  if (N == xm*ym*zm*dof) {
    gxm = xm;
    gym = ym;
    gzm = zm;
    gxs = xs;
    gys = ys;
    gzs = zs;
  } else if (N != gxm*gym*gzm*dof) SETERRQ3(PETSC_COMM_SELF,PETSC_ERR_ARG_INCOMP,"Vector local size %D is not compatible with DMDA local sizes %D %D\n",N,xm*ym*zm*dof,gxm*gym*gzm*dof);

  if (dim == 2) {
    ierr = VecRestoreArray3dView(vec,PKDEMO_BUFFER_FACTOR,gym,gxm*dof,0,gys,gxs*dof,(PetscScalar****)array);CHKERRQ(ierr);
  } else SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMDA dimension not 2, it is %D\n",dim);
  PetscFunctionReturn(0);
}
